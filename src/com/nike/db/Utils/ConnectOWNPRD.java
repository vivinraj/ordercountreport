package com.nike.db.Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectOWNPRD {
	private static Connection connection = null;

	private ConnectOWNPRD() {
		try {
			// Load the JDBC driver
			String driverName = "oracle.jdbc.driver.OracleDriver";
			Class.forName(driverName);
			String DB_URL = "jdbc:oracle:thin:@//ora-bi-rac-scan.sj.b2c.nike.com:1521/DCBIPRDCOMMON";
			String username = "COMM_PROD_SUPPORT";
			String password = "ps1asup0rtm#a6";

			// String username = "pmadh1";
			// String password = "jtimber";
			System.out.println("getting connection");
			connection = DriverManager
					.getConnection(DB_URL, username, password);
			System.out.println("Successfully Connected DB");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		// System.out.println("starting  connection");
		getConnection();
		closeConnection();
	}

	public static Connection getConnection() {
		// if(connection == null){
		new ConnectOWNPRD();
		// }
		return connection;
	}

	public static void closeConnection() {
		try {
			if (connection != null) {
				// System.out.println("Closing Connection");
				connection.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
