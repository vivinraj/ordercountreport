package com.nike.report.Bean;


public class OrderCountOneWeekVO {
	private int orderCount;
	private String orderDate;
	private String statusDescription;
	private String shipnodeKey;

	public int getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getShipnodeKey() {
		return shipnodeKey;
	}

	public void setShipnodeKey(String shipnodeKey) {
		this.shipnodeKey = shipnodeKey;
	}
}
