package com.nike.report.action;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
//import org.apache.log4j.Logger;
//import org.quartz.Job;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;

import com.nike.report.Chart.BARChartReportMain;
import com.nike.report.Chart.BARChartReportTotalWeeklyMain;

//public class OrderCountReportCreation implements Job {
//
//	@Override
//	public void execute(JobExecutionContext arg0) throws JobExecutionException {
public class OrderCountReportCreation {
	public static void main(String[] args) throws ParseException, IOException {
		// Logger logger = Logger.getLogger(this.getClass());
		// TODO Auto-generated method stub

		try {
			// String pattern = "dd-MM-yyyy";

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Calendar cal = Calendar.getInstance();
			System.out.println(cal.getTime());
			TimeZone currentTimeZone = cal.getTimeZone();
			int offset = currentTimeZone.getOffset(cal.getTimeInMillis());
			Date adjustedTime = new Date(cal.getTimeInMillis() - offset);
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			System.out.println(dateFormat.format(adjustedTime));

			String dateInString = dateFormat.format(adjustedTime).toString();
			System.out.println("Date for the Report: " + dateInString);

			System.out
					.println("Report get Started with executing the Combined Graph for last one week");
			BARChartReportMain barChartReportMain = new BARChartReportMain(
					"String", dateInString);

			System.out
					.println("Report get Started with executing the Hourly Graph for the day");
			BARChartReportHourlyMain barChartReportHourlyMain = new BARChartReportHourlyMain(
					"String", dateInString);

			System.out
					.println("Report get Started with executing the Total Order Count Released to All DC's");
			BARChartReportTotalWeeklyMain BARChartReportTotalWeeklyMain = new BARChartReportTotalWeeklyMain(
					"String", dateInString);

			System.out
					.println("Report get Started with executing the Email Creation part");
			sendEmailReport emailCreation = new sendEmailReport();

			emailCreation.sendEmail();

		} catch (AddressException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
