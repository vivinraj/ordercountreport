package com.nike.report.action;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import org.apache.log4j.Logger;
import com.nike.report.DAO.OrderCountHourlyDAO;

public class sendEmailReport {

	public void sendEmail() throws AddressException, MessagingException,
			IOException {

		// Logger logger = Logger.getLogger(this.getClass());
		StringBuffer message = new StringBuffer();
		OrderCountHourlyDAO Ordercount = new OrderCountHourlyDAO();
		String orderCountTable = Ordercount.getOrderCountTable();
		message.append("<html>");
		message.append("<font color=black> Hello Candice, <br><br> Please find the below order count report for the day. </font> <br><br>");
		message.append("<body>")
				.append("<b> <u> Total Order Drop to Warehouses by DC </b></u><br><br><br>")
				.append(orderCountTable)
				.append("<br><br>")
				.append("<b> <u> Total Number of Orders Released to DC's for One Week </b></u><br><br>");
		message.append("<img src=\"cid:bar-chart-all-dc@nike.com\"/>");
		message.append("<br><br>")
				.append("<b> <u> Combined bar chart with the 7-day trend of orders sent, orders acknowledged, orders shipped by DC </b></u><br><br>");
		message.append("<img src=\"cid:bar-chart-brd-graph@nike.com\"/>");
		message.append("<br><br>");

		message.append("<img src=\"cid:bar-chart-sd1-graph@nike.com\"/>");
		message.append("<br><br>");
		message.append("<img src=\"cid:bar-chart-nalc-graph@nike.com\"/>");

		message.append("<br><br>")
				.append("<b> <u> Hourly chart of order flow and trend by DC </b></u><br><br>");
		message.append("<img src=\"cid:bar-chart-brd-hourly@nike.com\"/>");
		message.append("<br><br><br><br>");
		message.append("<img src=\"cid:bar-chart-sd1-hourly@nike.com\"/>");
		message.append("<br><br><br><br>");
		message.append("<img src=\"cid:bar-chart-nalc-hourly@nike.com\"/>");
		message.append("<br><br><br><br>").append("Thank you")
				.append("<br><br>").append("NIKE Commerce PS Team")
				.append("</body>").append("</html>");

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-dd");

		TimeZone currentTimeZone = cal.getTimeZone();
		int offset = currentTimeZone.getOffset(cal.getTimeInMillis());
		Date adjustedTime = new Date(cal.getTimeInMillis() - offset);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		String dateInString = dateFormat.format(adjustedTime).toString();
		// String[] stringSplit = tempString.split("-");
		// StringBuffer dateInString = new StringBuffer();
		// dateInString.append(stringSplit[0]).append("-")
		// .append(Integer.parseInt(stringSplit[1]) - 1);
		System.out.println(dateInString);

		String host = "smtp.va2.b2c.nike.com";
		// String host = "mail.nike.com";
		String port = "25";
		final String mailFrom = "a.cdtpsauto@nike.com";
		final String password = "s5UqespA";
		String subject = "Order Count Report - " + dateInString;

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.user", mailFrom);
		properties.put("mail.password", password);
		Authenticator auth = new Authenticator() {
			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailFrom, password);
			}
		}; /* Unavailable Anonymous Inner Class!! */

		Session session = Session.getInstance(properties, auth);
		MimeMessage msg = new MimeMessage(session);

		// InternetAddress[] to = new InternetAddress[] { new InternetAddress(
		// "Candice.Tang@nike.com") };
		// InternetAddress[] cc = new InternetAddress[] {
		// new InternetAddress("Hudson.Genovese@nike.com"),
		// new InternetAddress("Ramesh.Ponraj@nike.com"),
		// new InternetAddress("Ashish.Sharma@nike.com"),
		// new InternetAddress(
		// "Satheeshkumar.Kattavurchandrasekaran@nike.com"),
		// new InternetAddress("Vinupradha.R@nike.com"),
		// new InternetAddress("Kiran.Tirupathi@nike.com"),
		// new InternetAddress("Vivinraj.Sundararaj@nike.com"),
		// new InternetAddress("Sarankumar.Gopalakrishnan@nike.com") };

		InternetAddress[] to = new InternetAddress[] { new InternetAddress(
				"Vivinraj.Sundararaj@nike.com") };

		InternetAddress[] cc = new InternetAddress[] { new InternetAddress(
				"Sarankumar.Gopalakrishnan@nike.com") };

		msg.setFrom(new InternetAddress(mailFrom));
		msg.setRecipients(Message.RecipientType.TO, to);
		msg.setRecipients(Message.RecipientType.CC, cc);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		MimeBodyPart messageBodyPart = new MimeBodyPart();

		messageBodyPart.setContent(message.toString(), "text/html");
		messageBodyPart.setHeader("Content-Type", "text/html");

		MimeMultipart multipart = new MimeMultipart("related");
		multipart.addBodyPart(messageBodyPart);

		String inputFile = "/cust/home/a.cdtpsauto/OrderReportAutomation/generateChart.properties";
		final Properties prop = new Properties();
		// ClassLoader classLoader = Thread.currentThread()
		// .getContextClassLoader();
		// InputStream input = classLoader.getResourceAsStream(inputFile1);
		prop.load(new FileInputStream(inputFile));
		String outputLocation = prop.getProperty("outputLocation");

		DataSource fds0 = new FileDataSource(outputLocation
				+ "BarChart_ALL_DC.png");
		DataSource fds1 = new FileDataSource(outputLocation
				+ "BarChart_BRD.png");
		DataSource fds2 = new FileDataSource(outputLocation
				+ "BarChart_LDC.png");
		DataSource fds3 = new FileDataSource(outputLocation
				+ "BarChart_NALC.png");
		DataSource fds4 = new FileDataSource(outputLocation
				+ "BarChart_BRD_Hourly.png");
		DataSource fds5 = new FileDataSource(outputLocation
				+ "BarChart_LDC_Hourly.png");
		DataSource fds6 = new FileDataSource(outputLocation
				+ "BarChart_NALC_Hourly.png");

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds0));
		messageBodyPart.setHeader("Content-ID", "<bar-chart-all-dc@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-all-dc@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds1));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-brd-graph@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-brd-graph@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds2));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-sd1-graph@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-sd1-graph@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds3));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-nalc-graph@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-nalc-graph@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds4));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-brd-hourly@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-brd-hourly@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds5));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-sd1-hourly@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-sd1-hourly@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(fds6));
		messageBodyPart.setHeader("Content-ID",
				"<bar-chart-nalc-hourly@nike.com>");
		messageBodyPart.addHeader("Content-Type", "image/png");
		messageBodyPart.setContentID("<bar-chart-nalc-hourly@nike.com>");
		messageBodyPart.setDisposition(MimeBodyPart.INLINE);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		String attachFile1 = outputLocation + "OrderCount_Comparison.xls";
		String fileName1 = "Comparison_Count__All_DC's" + ".xls";
		DataSource source = new FileDataSource(attachFile1);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(fileName1);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		String attachFile2 = outputLocation + "OrderCount_Hourly.xls";
		String fileName2 = "Hourly_Count" + ".xls";
		DataSource source1 = new FileDataSource(attachFile2);
		messageBodyPart.setDataHandler(new DataHandler(source1));
		messageBodyPart.setFileName(fileName2);
		multipart.addBodyPart(messageBodyPart);

		messageBodyPart = new MimeBodyPart();
		String attachFile3 = outputLocation + "OrderCount_Total_Count.xls";
		String fileName3 = "Total_Count__All_DC's" + ".xls";
		DataSource source3 = new FileDataSource(attachFile3);
		messageBodyPart.setDataHandler(new DataHandler(source3));
		messageBodyPart.setFileName(fileName3);
		multipart.addBodyPart(messageBodyPart);

		MimeMultipart mpMixed = new MimeMultipart("mixed");
		MimeBodyPart relatedInMixed = new MimeBodyPart();
		relatedInMixed.setContent(multipart);
		mpMixed.addBodyPart(relatedInMixed);

		msg.setContent(multipart);
		System.out.println("Mail Prepared successfully");
		Transport.send(msg);
		System.out.println("Mail triggered to the recipients successfully");
	}
}