package com.nike.report.Chart;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import com.nike.report.Bean.OrderCountTotalVO;
import com.nike.report.DAO.OrdercountTotalDAO;

public class BARChartReportTotalWeeklyMain {
	private static final long serialVersionUID = 1L;
	private List<List<OrderCountTotalVO>> warehouseQueryList = new ArrayList<List<OrderCountTotalVO>>();
	private DefaultCategoryDataset dataset = new DefaultCategoryDataset();

	/**
	 * 077 * Creates a new demo instance. 078 * 079 * @param title the frame
	 * title. 080
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */
	public BARChartReportTotalWeeklyMain(String title, String getDate)
			throws ParseException, IOException {
		super();
		// for Warehouses

		String[] Warehouse = { "BRD", "LDC", "NALC" };
		for (int i = 0; i < Warehouse.length; i++) {
			warehouseQueryList.add(createDataset(Warehouse[i], getDate));
		}

		for (int i = 0; i < warehouseQueryList.size(); i++) {
			List<OrderCountTotalVO> tempList = warehouseQueryList.get(i);
			for (OrderCountTotalVO bean : tempList) {
				dataset.addValue(bean.getOrderCount(),
						bean.getStatusDescription(), bean.getOrderDate());
			}
		}
		createChart(dataset);
	}

	/**
	 * 093 * Returns a sample dataset. 094 * 095 * @return The dataset. 096
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */
	private static List<OrderCountTotalVO> createDataset(String Warehouse,
			String getDate) throws ParseException, IOException {
		OrdercountTotalDAO orderCountWeek = new OrdercountTotalDAO();
		List<OrderCountTotalVO> queryList = new ArrayList<OrderCountTotalVO>();
		if (Warehouse.equalsIgnoreCase("BRD")) {
			List<OrderCountTotalVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountTotalVO bean = queryResultList.get(i);
				queryList.add(bean);
			}
			String WareHouseSheetName = "Total Count (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountTotalBRD,
					WareHouseSheetName);
		} else if (Warehouse.equalsIgnoreCase("LDC")) {
			List<OrderCountTotalVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountTotalVO bean = queryResultList.get(i);
				queryList.add(bean);
			}
			String WareHouseSheetName = "Total Count (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountTotalLDC,
					WareHouseSheetName);
		} else if (Warehouse.equalsIgnoreCase("NALC")) {
			List<OrderCountTotalVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountTotalVO bean = queryResultList.get(i);
				queryList.add(bean);
			}
			String WareHouseSheetName = "Total Count (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountTotalNALC,
					WareHouseSheetName);
		}
		return queryList;
	}

	/**
	 * 107 * Creates a sample chart. 108 * 109 * @param dataset the dataset. 110
	 * * 111 * @return The chart. 112
	 */

	private JFreeChart createChart(DefaultCategoryDataset dataset2) {

		JFreeChart chart = ChartFactory.createBarChart("BRD VS SD1 VS NALC",
				"One Week daily Basis", "Total Order Count (IN Numbers)",
				dataset2);

		chart.setBackgroundPaint(Color.white);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		plot.setBackgroundPaint(Color.white);
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		final BarRenderer renderer = (BarRenderer) plot.getRenderer();
		CategoryAxis categoryaxis = plot.getDomainAxis();
		categoryaxis.setCategoryMargin(0.1);// whole category formatter
		renderer.setItemMargin(0.001); // single bar formatter

		renderer.setDrawBarOutline(true);

		renderer.setSeriesItemLabelGenerator(
				0,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));
		renderer.setSeriesItemLabelGenerator(
				1,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));
		renderer.setSeriesItemLabelGenerator(
				2,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));

		renderer.setSeriesItemLabelsVisible(0, true);
		renderer.setSeriesItemLabelsVisible(1, true);
		renderer.setSeriesItemLabelsVisible(2, true);
		renderer.setBaseItemLabelsVisible(true);
		chart.getCategoryPlot().setRenderer(renderer);

		try {
			String inputFile = "/cust/home/a.cdtpsauto/OrderReportAutomation/generateChart.properties";
			final Properties prop = new Properties();
			// ClassLoader classLoader = Thread.currentThread()
			// .getContextClassLoader();
			// InputStream inputStream = classLoader
			// .getResourceAsStream(inputFile);
			// prop.load(inputStream);
			prop.load(new FileInputStream(inputFile));
			String pattern = "MMMdd-yy HH-mm-ss";
			String dateInString = new SimpleDateFormat(pattern)
					.format(new Date());
			String outputLocation = prop.getProperty("outputLocation")
					+ "BarChart_ALL_DC" + ".png";

			ChartUtilities.saveChartAsPNG(new File(outputLocation), chart,
					1048, 490);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return chart;
	}

	public static void main(String[] args) throws ParseException, IOException {

		new BARChartReportTotalWeeklyMain(
				"JFreeChart: BARChartReportTotalWeeklyMain.java", "");
	}
}
