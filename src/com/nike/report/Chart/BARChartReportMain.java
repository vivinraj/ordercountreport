package com.nike.report.Chart;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;

import com.nike.report.Bean.OrderCountOneWeekVO;
import com.nike.report.DAO.OrderCountOneWeekDAO;

public class BARChartReportMain {

	private static final long serialVersionUID = 1L;

	/**
	 * 077 * Creates a new demo instance. 078 * 079 * @param title the frame
	 * title. 080
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */

	public BARChartReportMain(String title, String getDate)
			throws ParseException, IOException {
		super();
		// for Warehouses
		String[] Warehouse = { "BRD", "LDC", "NALC" };
		for (int i = 0; i < Warehouse.length; i++) {
			DefaultCategoryDataset dataset = createDataset(Warehouse[i],
					getDate);
			// boolean sortRows;
			// this.dataset = new DefaultKeyedValues2D(sortRows);
			createChart(dataset, Warehouse[i]);
		}
	}

	/**
	 * 093 * Returns a sample dataset. 094 * 095 * @return The dataset. 096
	 * 
	 * @throws ParseException
	 * @throws IOException
	 */
	private static DefaultCategoryDataset createDataset(String Warehouse,
			String getDate) throws ParseException, IOException {
		OrderCountOneWeekDAO orderCountWeek = new OrderCountOneWeekDAO();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		if (Warehouse.equalsIgnoreCase("BRD")) {
			List<OrderCountOneWeekVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountOneWeekVO bean = queryResultList.get(i);
				dataset.addValue(bean.getOrderCount(),
						bean.getStatusDescription(), bean.getOrderDate());
			}
			String WareHouseSheetName = "Comparison (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountWeekBRD,
					WareHouseSheetName);
		} else if (Warehouse.equalsIgnoreCase("LDC")) {
			List<OrderCountOneWeekVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountOneWeekVO bean = queryResultList.get(i);
				dataset.addValue(bean.getOrderCount(),
						bean.getStatusDescription(), bean.getOrderDate());
			}
			String WareHouseSheetName = "Comparison (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountWeekLDC,
					WareHouseSheetName);
		} else if (Warehouse.equalsIgnoreCase("NALC")) {
			List<OrderCountOneWeekVO> queryResultList = orderCountWeek
					.queryResult(Warehouse, getDate);
			for (int i = 0; i < queryResultList.size(); i++) {
				OrderCountOneWeekVO bean = queryResultList.get(i);
				dataset.addValue(bean.getOrderCount(),
						bean.getStatusDescription(), bean.getOrderDate());
			}
			String WareHouseSheetName = "Comparison (" + Warehouse + ")";
			orderCountWeek.writeDataToExcel(orderCountWeek.orderCountWeekNALC,
					WareHouseSheetName);
		}

		return dataset;
	}

	/**
	 * 107 * Creates a sample chart. 108 * 109 * @param dataset the dataset. 110
	 * * 111 * @return The chart. 112
	 */
	private JFreeChart createChart(DefaultCategoryDataset dataset,
			String Warehouse) {

		// In create chart fn param1 is heading,param2 is x-axis label and
		// param3 is y-axis label

		String newWarehouse = "";
		if (Warehouse.equalsIgnoreCase("LDC")) {
			newWarehouse = "SD1";
		} else {
			newWarehouse = Warehouse;
		}

		JFreeChart chart = ChartFactory.createBarChart(newWarehouse,
				"One Week daily Basis", "Total Order Count (IN Numbers)",
				dataset);
		chart.addSubtitle(new TextTitle("Released VS Acknowledged VS Shipped"));
		chart.setBackgroundPaint(Color.white);
		CategoryPlot plot = (CategoryPlot) chart.getPlot();
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		plot.setBackgroundPaint(Color.white);
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		final BarRenderer renderer = (BarRenderer) plot.getRenderer();
		CategoryAxis categoryaxis = plot.getDomainAxis();
		categoryaxis.setCategoryMargin(0.1);// whole category formatter
		renderer.setItemMargin(0.001); // single bar formatter

		renderer.setDrawBarOutline(true);

		renderer.setSeriesItemLabelGenerator(
				0,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));
		renderer.setSeriesItemLabelGenerator(
				1,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));
		renderer.setSeriesItemLabelGenerator(
				2,
				new StandardCategoryItemLabelGenerator("{2}", NumberFormat
						.getNumberInstance()));

		renderer.setSeriesItemLabelsVisible(0, true);
		renderer.setSeriesItemLabelsVisible(1, true);
		renderer.setSeriesItemLabelsVisible(2, true);
		renderer.setBaseItemLabelsVisible(true);
		chart.getCategoryPlot().setRenderer(renderer);

		try {
			// ClassLoader classLoader =
			// Thread.currentThread().getContextClassLoader();
			// InputStream inputFile = classLoader.getResourceAsStream("")
			String inputFile = "/cust/home/a.cdtpsauto/OrderReportAutomation/generateChart.properties";
			final Properties prop = new Properties();
			// ClassLoader classLoader = Thread.currentThread()
			// .getContextClassLoader();
			// InputStream inputStream = classLoader
			// .getResourceAsStream(inputFile);
			// prop.load(inputStream);
			prop.load(new FileInputStream(inputFile));
			String pattern = "MMMdd-yy HH-mm-ss";
			String dateInString = new SimpleDateFormat(pattern)
					.format(new Date());
			String outputLocation = prop.getProperty("outputLocation")
					+ "BarChart_" + Warehouse + ".png";

			ChartUtilities.saveChartAsPNG(new File(outputLocation), chart,
					1048, 490);
			// ChartUtilities.saveChartAsPNG(
			// new File(prop.getProperty("outputLocation")), chart, 2000,
			// 900);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return chart;
	}

	public static void main(String[] args) throws ParseException, IOException {

		new BARChartReportMain("JFreeChart: BARChartReportMain.java", "");
	}
}
