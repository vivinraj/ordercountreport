package com.nike.report.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

//import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.nike.db.Utils.ConnectOWNPRD;
import com.nike.report.Bean.OrderCountOneWeekVO;

public class OrderCountOneWeekDAO {

	public static List orderCountWeekBRD = new ArrayList();
	public static List orderCountWeekLDC = new ArrayList();
	public static List orderCountWeekNALC = new ArrayList();
	public static List OrderCountWeek = new ArrayList();

	public static HSSFWorkbook comparisonWorkBook = new HSSFWorkbook();
	public static HSSFRow comparisonRow = null;
	public static HSSFCell comparisonCell = null;

	public static int row = 0;

	private static int j = 0;

	public List<OrderCountOneWeekVO> queryResult(String Warehouse,
			String getDate) throws ParseException, IOException {
		// Logger logger = Logger.getLogger(this.getClass());
		if (Warehouse.equalsIgnoreCase("BRD")) {

			try {
				PreparedStatement pStatement = null;
				ResultSet result = null;

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -8);
				String fromDateIfConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, +1);
				String fromDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] toDateSplit = toDateConversion.split("-");
				String[] fromDateIfSplit = fromDateIfConversion.split("-");
				String[] fromDateSplit = fromDateConversion.split("-");

				StringBuffer fromDate = new StringBuffer();
				StringBuffer toDate = new StringBuffer();

				StringBuffer fromDateIf = new StringBuffer();
				StringBuffer toDateIf = new StringBuffer();

				fromDateIf.append(fromDateIfSplit[0]).append("-")
						.append(fromDateIfSplit[1]).append("-")
						.append(fromDateIfSplit[2]);

				toDateIf.append(toDateSplit[0]).append("-")
						.append(toDateSplit[1]).append("-")
						.append(toDateSplit[2]);

				fromDate.append(fromDateSplit[2]).append(fromDateSplit[1])
						.append(fromDateSplit[0]).append("08");
				toDate.append(toDateSplit[2]).append(toDateSplit[1])
						.append(toDateSplit[0]).append("08");

				Connection con = ConnectOWNPRD.getConnection();
				System.out
						.println("Running the query to fetch the Total Order count for BRD");
				StringBuilder query = new StringBuilder(
						"SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query.append(" AND HDR.document_type = '0001' AND ST.status  in('3700','3200.03','3200.100') AND REL.shipnode_key in ('BRD') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement = con.prepareStatement(query.toString());
				pStatement.setString(1, fromDate.toString());
				pStatement.setString(2, toDate.toString());

				result = pStatement.executeQuery();
				System.out
						.println("Fetching the results for the Total Count of Orders for BRD");

				while (result.next()) {
					String DOMSStatus = result.getString("shipnode_key");
					boolean present = false;
					OrderCountOneWeekVO ordercountobj = new OrderCountOneWeekVO();
					ordercountobj.setOrderCount(Integer.parseInt(result
							.getString("ORDER_COUNT")));
					if (dateFormat(result.getString("ORDER_DATE").toString())
							.equals(fromDateIf.toString())
							|| dateFormat(
									result.getString("ORDER_DATE").toString())
									.equals(toDateIf.toString())) {

					} else {
						ordercountobj.setOrderDate(dateFormat(result.getString(
								"ORDER_DATE").toString()));
						if (result.getString("Status_Desc").equalsIgnoreCase(
								"Published to WMS")) {
							ordercountobj
									.setStatusDescription("Released_TO_DC");
						} else if (result.getString("Status_Desc")
								.equalsIgnoreCase("Acknowledged")) {
							ordercountobj
									.setStatusDescription("Warehouse_Acknowledged");
						} else if (result.getString("Status_Desc")
								.equalsIgnoreCase("Shipped")) {
							ordercountobj
									.setStatusDescription("Warehouse_Shipped");
						}
						ordercountobj.setShipnodeKey(result
								.getString("shipnode_key"));

						orderCountWeekBRD.add(ordercountobj);
					}
				}
				con.close();
				pStatement.close();
				result.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
			}
		} else if (Warehouse.equalsIgnoreCase("LDC")) {
			try {
				PreparedStatement pStatement1 = null;
				ResultSet result1 = null;

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -8);
				String fromDateIfConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, +1);
				String fromDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] toDateSplit = toDateConversion.split("-");
				String[] fromDateIfSplit = fromDateIfConversion.split("-");
				String[] fromDateSplit = fromDateConversion.split("-");

				StringBuffer fromDate = new StringBuffer();
				StringBuffer toDate = new StringBuffer();

				StringBuffer fromDateIf = new StringBuffer();
				StringBuffer toDateIf = new StringBuffer();

				fromDateIf.append(fromDateIfSplit[0]).append("-")
						.append(fromDateIfSplit[1]).append("-")
						.append(fromDateIfSplit[2]);

				toDateIf.append(toDateSplit[0]).append("-")
						.append(toDateSplit[1]).append("-")
						.append(toDateSplit[2]);

				fromDate.append(fromDateSplit[2]).append(fromDateSplit[1])
						.append(fromDateSplit[0]).append("08");
				toDate.append(toDateSplit[2]).append(toDateSplit[1])
						.append(toDateSplit[0]).append("08");

				Connection con1 = ConnectOWNPRD.getConnection();
				System.out
						.println("Running the query to fetch the Total Order count for LDC");
				StringBuilder query1 = new StringBuilder(
						"SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query1.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query1.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query1.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query1.append(" AND HDR.document_type = '0001' AND ST.status  in('3700','3200.03','3200.100') AND REL.shipnode_key in ('LDC') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query1.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query1.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query1.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement1 = con1.prepareStatement(query1.toString());
				pStatement1.setString(1, fromDate.toString());
				pStatement1.setString(2, toDate.toString());

				result1 = pStatement1.executeQuery();
				System.out
						.println("Fetching the results for the Total Count of Orders for LDC");
				while (result1.next()) {
					OrderCountOneWeekVO ordercountobj = new OrderCountOneWeekVO();
					ordercountobj.setOrderCount(Integer.parseInt(result1
							.getString("ORDER_COUNT")));
					if (dateFormat(result1.getString("ORDER_DATE").toString())
							.equals(fromDateIf.toString())
							|| dateFormat(
									result1.getString("ORDER_DATE").toString())
									.equals(toDateIf.toString())) {

					} else {
						ordercountobj.setOrderDate(dateFormat(result1
								.getString("ORDER_DATE").toString()));
						if (result1.getString("Status_Desc").equalsIgnoreCase(
								"Published to WMS")) {
							ordercountobj
									.setStatusDescription("Released_to_DC");
						} else if (result1.getString("Status_Desc")
								.equalsIgnoreCase("Acknowledged")) {
							ordercountobj
									.setStatusDescription("Warehouse_Acknowledged");
						} else if (result1.getString("Status_Desc")
								.equalsIgnoreCase("Shipped")) {
							ordercountobj
									.setStatusDescription("Warehouse_Shipped");
						}
						ordercountobj.setShipnodeKey(result1
								.getString("shipnode_key"));
						orderCountWeekLDC.add(ordercountobj);
					}

				}
				con1.close();
				pStatement1.close();
				result1.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {

			}
		} else if (Warehouse.equalsIgnoreCase("NALC")) {
			try {
				PreparedStatement pStatement2 = null;
				ResultSet result2 = null;

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -8);
				String fromDateIfConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, +1);
				String fromDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] toDateSplit = toDateConversion.split("-");
				String[] fromDateIfSplit = fromDateIfConversion.split("-");
				String[] fromDateSplit = fromDateConversion.split("-");

				StringBuffer fromDate = new StringBuffer();
				StringBuffer toDate = new StringBuffer();

				StringBuffer fromDateIf = new StringBuffer();
				StringBuffer toDateIf = new StringBuffer();

				fromDateIf.append(fromDateIfSplit[0]).append("-")
						.append(fromDateIfSplit[1]).append("-")
						.append(fromDateIfSplit[2]);

				toDateIf.append(toDateSplit[0]).append("-")
						.append(toDateSplit[1]).append("-")
						.append(toDateSplit[2]);

				fromDate.append(fromDateSplit[2]).append(fromDateSplit[1])
						.append(fromDateSplit[0]).append("08");
				toDate.append(toDateSplit[2]).append(toDateSplit[1])
						.append(toDateSplit[0]).append("08");

				Connection con2 = ConnectOWNPRD.getConnection();
				System.out
						.println("Running the query to fetch the Total Order count for NALC");
				StringBuilder query2 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ COUNT(DISTINCT ORH.ORDER_NO) AS ORDER_COUNT,ST.DESCRIPTION AS Status_Desc,to_char(ORS.status_date-8/24,'yyyy-mm-dd') AS ORDER_DATE ");
				query2.append(" FROM DOM.YFS_ORDER_RELEASE_STATUS ORS, DOM.YFS_ORDER_LINE ORL, DOM.YFS_ORDER_HEADER ORH, DOM.YFS_STATUS ST ");
				query2.append(" WHERE  ORS.ORDER_HEADER_KEY = ORH.ORDER_HEADER_KEY AND ORS.ORDER_LINE_KEY = ORL.ORDER_LINE_KEY AND ORS.STATUS = ST.STATUS ");
				query2.append(" AND ST.PROCESS_TYPE_KEY = 'PO_FULFILLMENT' AND ORH.DOCUMENT_TYPE = '0005' ");
				query2.append(" and ORS.ORDER_RELEASE_STATUS_KEY > ? and ORS.ORDER_RELEASE_STATUS_KEY < ? ");
				query2.append(" AND ORS.STATUS <> '1400' ");
				query2.append(" and ors.status in ('1100.20','1100.250','3700') AND ORL.SHIPNODE_KEY LIKE '1014%' ");
				query2.append(" AND ORL.LINE_TYPE = 'INLINE' ");
				query2.append(" GROUP BY ST.DESCRIPTION,to_char(ORS.status_date-8/24,'yyyy-mm-dd') ORDER BY ORDER_DATE ");

				pStatement2 = con2.prepareStatement(query2.toString());
				pStatement2.setString(1, fromDate.toString());
				pStatement2.setString(2, toDate.toString());

				result2 = pStatement2.executeQuery();
				System.out
						.println("Fetching the results for the Total Count of Orders for NALC");

				while (result2.next()) {
					OrderCountOneWeekVO ordercountobj = new OrderCountOneWeekVO();
					ordercountobj.setOrderCount(Integer.parseInt(result2
							.getString("ORDER_COUNT")));
					if (dateFormat(result2.getString("ORDER_DATE").toString())
							.equals(fromDateIf.toString())
							|| dateFormat(
									result2.getString("ORDER_DATE").toString())
									.equals(toDateIf.toString())) {

					} else {
						ordercountobj.setOrderDate(dateFormat(result2
								.getString("ORDER_DATE").toString()));
						if (result2.getString("Status_Desc").equalsIgnoreCase(
								"Released to Fulfiller")) {
							ordercountobj
									.setStatusDescription("Released_to_DC");
						} else if (result2.getString("Status_Desc")
								.equalsIgnoreCase("Warehouse Acknowledged")) {
							ordercountobj
									.setStatusDescription("Warehouse_Acknowledged");
						} else if (result2.getString("Status_Desc")
								.equalsIgnoreCase("Shipped")) {
							ordercountobj
									.setStatusDescription("Warehouse_Shipped");
						}
						ordercountobj.setShipnodeKey("NALC");
						orderCountWeekNALC.add(ordercountobj);
					}
				}
				con2.close();
				pStatement2.close();
				result2.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
			}
		}
		List OrderCountWeek = new ArrayList();
		if (Warehouse.equalsIgnoreCase("BRD")) {
			OrderCountWeek.addAll(orderCountWeekBRD);
		} else if (Warehouse.equalsIgnoreCase("LDC")) {
			OrderCountWeek.addAll(orderCountWeekLDC);
		} else if (Warehouse.equalsIgnoreCase("NALC")) {
			OrderCountWeek.addAll(orderCountWeekNALC);
		}

		return OrderCountWeek;
	}

	public String dateFormat(String str_date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newformat = new SimpleDateFormat("dd-MM-yyyy");

		Date date = formatter.parse(str_date);
		String resultDate = newformat.format(date);
		return resultDate;
	}

	public static void createHeader(HSSFSheet mySheet) {
		short sh = 0;
		HSSFRow comparisonRow = mySheet.createRow(0);
		HSSFCell comparisonCell = comparisonRow.createCell(sh);
		comparisonCell.setCellValue("ORDER COUNT");
		comparisonCell = comparisonRow.createCell(++sh);
		comparisonCell.setCellValue("ORDER DATE");
		comparisonCell = comparisonRow.createCell(++sh);
		comparisonCell.setCellValue("STATUS DESC");
		comparisonCell = comparisonRow.createCell(++sh);
		comparisonCell.setCellValue("SHIPNODE KEY");
	}

	public static void writeDataToExcel(List orderCountWeekExcel,
			String WareHouseSheetName) {
		int row = 0;
		HSSFSheet mySheet = comparisonWorkBook.createSheet(WareHouseSheetName);
		short defaultColWidth = 20;
		mySheet.setDefaultColumnWidth(defaultColWidth);
		short sh = 0;
		createHeader(mySheet);

		for (int i = 0; i < orderCountWeekExcel.size(); i++) {
			OrderCountOneWeekVO obj = (OrderCountOneWeekVO) orderCountWeekExcel
					.get(i);
			comparisonRow = mySheet.createRow(++row);
			comparisonCell = comparisonRow.createCell(sh);
			comparisonCell.setCellValue(obj.getOrderCount());
			comparisonCell = comparisonRow.createCell(++sh);
			comparisonCell.setCellValue(obj.getOrderDate());
			comparisonCell = comparisonRow.createCell(++sh);
			comparisonCell.setCellValue(obj.getStatusDescription());
			comparisonCell = comparisonRow.createCell(++sh);
			comparisonCell.setCellValue(obj.getShipnodeKey());
			comparisonCell = comparisonRow.createCell(++sh);
			sh = 0;
		}
		FileOutputStream fileOut = null;
		try {
			String inputFile = "/cust/home/a.cdtpsauto/OrderReportAutomation/generateChart.properties";
			// String inputFile = "resources/generateChart.properties";
			final Properties prop = new Properties();
			// ClassLoader classLoader = Thread.currentThread()
			// .getContextClassLoader();
			// InputStream inputStream = classLoader
			// .getResourceAsStream(inputFile);
			// prop.load(inputStream);

			prop.load(new FileInputStream(inputFile));
			String OutputLocation = prop.getProperty("outputLocation");
			Boolean flag = false;
			if (!flag) {
				FileOutputStream out = new FileOutputStream(new File(
						OutputLocation + "OrderCount_Comparison.xls"));
				comparisonWorkBook.write(out);
				out.close();
				System.out.println("File Generated Successfully!");
				flag = true;
			} else {
				System.out.println("File NOT Generated Successfully!");
			}

		} catch (Exception e) {
			// logger.error(e);
			e.printStackTrace();
		}
	}
}
