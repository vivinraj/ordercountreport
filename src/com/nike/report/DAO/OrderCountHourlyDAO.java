package com.nike.report.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

//import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.nike.db.Utils.ConnectOWNPRD;
import com.nike.report.Bean.OrderCountHourlyVO;

public class OrderCountHourlyDAO {

	// private static Connection connection = null;
	// Logger logger = Logger.getLogger(this.getClass());
	public static List orderCountHourlyBRDToday = new ArrayList();
	public static List orderCountHourlyLDCToday = new ArrayList();
	public static List<OrderCountHourlyVO> orderCountHourlyNALCToday = new ArrayList<OrderCountHourlyVO>();

	public static List orderCountHourlyBRDYesterday = new ArrayList();
	public static List orderCountHourlyLDCYesterday = new ArrayList();
	public static List<OrderCountHourlyVO> orderCountHourlyNALCYesterday = new ArrayList<OrderCountHourlyVO>();

	public static List orderCountHourlyBRDLastWeek = new ArrayList();
	public static List orderCountHourlyLDCLastWeek = new ArrayList();
	public static List<OrderCountHourlyVO> orderCountHourlyNALCLastWeek = new ArrayList<OrderCountHourlyVO>();

	private static List<OrderCountHourlyVO> OrderCountHourly = new ArrayList<OrderCountHourlyVO>();

	public static int totalOrdercountBRD;
	public static int totalOrdercountLDC;
	public static int totalOrdercountNALC;

	private static int totalOrdercountBRDYesterday;
	private static int totalOrdercountLDCYesterday;
	private static int totalOrdercountNALCYesterday;

	private static int totalOrdercountBRDOneWeek;
	private static int totalOrdercountLDCOneWeek;
	private static int totalOrdercountNALCOneWeek;

	public static HSSFWorkbook hourlyCountWorkBook = new HSSFWorkbook();
	public static HSSFRow hourlyCountRow = null;
	public static HSSFCell hourlyCountCell = null;

	public List<OrderCountHourlyVO> queryResult(String Warehouse, String getDate)
			throws ParseException, IOException {
		if (Warehouse.equalsIgnoreCase("BRD")) {
			int TotalBRDcount = 0;
			int TotalBRDcountYesterday = 0;
			int TotalBRDcountOneWeek = 0;
			try {
				ResultSet result = null;
				PreparedStatement pStatement = null;

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] fromTodayDateSplit = fromTodayDateConversion
						.split("-");
				String[] toTodayDateSplit = toTodayDateConversion.split("-");

				StringBuffer fromTodayDate = new StringBuffer();
				StringBuffer toTodayDate = new StringBuffer();

				fromTodayDate.append(fromTodayDateSplit[2])
						.append(fromTodayDateSplit[1])
						.append(fromTodayDateSplit[0]).append("08");

				toTodayDate.append(toTodayDateSplit[2])
						.append(toTodayDateSplit[1])
						.append(toTodayDateSplit[0]).append("08");

				StringBuffer fromYesterdayDate = new StringBuffer();
				StringBuffer toYesterdayDate = new StringBuffer();

				String toYesterdayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromYesterdayDateConversion = myDate.format(
						cal.getTime()).toString();

				String[] fromYesterdayDateSplit = fromYesterdayDateConversion
						.split("-");
				String[] toYesterdayDateSplit = toYesterdayDateConversion
						.split("-");

				fromYesterdayDate.append(fromYesterdayDateSplit[2])
						.append(fromYesterdayDateSplit[1])
						.append(fromYesterdayDateSplit[0]).append("08");

				toYesterdayDate.append(toYesterdayDateSplit[2])
						.append(toYesterdayDateSplit[1])
						.append(toYesterdayDateSplit[0]).append("08");

				StringBuffer fromLastWeekDate = new StringBuffer();
				StringBuffer toLastWeekDate = new StringBuffer();

				cal.add(Calendar.DAY_OF_MONTH, -5);
				String toLastWeekDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromLastWeekDateConversion = myDate
						.format(cal.getTime()).toString();

				String[] fromLastWeekDateSplit = fromLastWeekDateConversion
						.split("-");
				String[] toLastWeekDateSplit = toLastWeekDateConversion
						.split("-");

				fromLastWeekDate.append(fromLastWeekDateSplit[2])
						.append(fromLastWeekDateSplit[1])
						.append(fromLastWeekDateSplit[0]).append("08");

				toLastWeekDate.append(toLastWeekDateSplit[2])
						.append(toLastWeekDateSplit[1])
						.append(toLastWeekDateSplit[0]).append("08");

				Connection con = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the query for BRD every one hour Today");

				StringBuilder query = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('BRD') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement = con.prepareStatement(query.toString());
				pStatement.setString(1, fromTodayDate.toString());
				pStatement.setString(2, toTodayDate.toString());

				result = pStatement.executeQuery();
				System.out
						.println("Fetching the results after running the query for BRD Today");

				while (result.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result
							.getString("ORDER_DATE")));
					orderCountHourlyBRDToday.add(ordercountobj);
					TotalBRDcount += ordercountobj.getOrderCount();

				}
				con.close();
				pStatement.close();
				result.close();

				totalOrdercountBRD = TotalBRDcount;
				String WareHouseSheetNameBRD = "Hourly Order Today ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyBRDToday,
						WareHouseSheetNameBRD);

				ResultSet result1 = null;
				PreparedStatement pStatement1 = null;

				Connection con1 = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the Comparison graph for BRD for every one hour Yesterday");
				StringBuilder query1 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query1.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query1.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query1.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query1.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('BRD') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query1.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query1.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query1.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement1 = con1.prepareStatement(query1.toString());
				pStatement1.setString(1, fromYesterdayDate.toString());
				pStatement1.setString(2, toYesterdayDate.toString());

				result1 = pStatement1.executeQuery();
				System.out
						.println("Fetching the results after running the query for BRD for very one hour Yesterday");

				while (result1.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result1
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result1
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result1
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result1
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result1
							.getString("ORDER_DATE")));
					orderCountHourlyBRDYesterday.add(ordercountobj);
					TotalBRDcountYesterday += ordercountobj.getOrderCount();

				}
				con1.close();
				pStatement1.close();
				result1.close();

				totalOrdercountBRDYesterday = TotalBRDcountYesterday;

				String WareHouseSheetNameBRDYesterday = "Hourly Order Yesterday ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyBRDYesterday,
						WareHouseSheetNameBRDYesterday);

				ResultSet result2 = null;
				PreparedStatement pStatement2 = null;

				Connection con2 = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the Comparison graph query for BRD for every hour last week the same day");
				StringBuilder query2 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query2.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query2.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query2.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query2.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('BRD') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query2.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query2.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ?");
				query2.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement2 = con2.prepareStatement(query2.toString());
				pStatement2.setString(1, fromLastWeekDate.toString());
				pStatement2.setString(2, toLastWeekDate.toString());

				result2 = pStatement2.executeQuery();
				System.out
						.println("Fetching the results after running the query for BRD for every hour last week the same day");

				while (result2.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result2
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result2
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result2
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result2
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result2
							.getString("ORDER_DATE")));
					orderCountHourlyBRDLastWeek.add(ordercountobj);
					TotalBRDcountOneWeek += ordercountobj.getOrderCount();
				}
				con2.close();
				pStatement2.close();
				result2.close();
				totalOrdercountBRDOneWeek = TotalBRDcountOneWeek;
				String WareHouseSheetNameBRDOneWeek = "Hourly Order LastWeek ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyBRDLastWeek,
						WareHouseSheetNameBRDOneWeek);

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
			}
		}

		else if (Warehouse.equalsIgnoreCase("LDC")) {

			int TotalLDCcount = 0;
			int TotalLDCcountYesterday = 0;
			int TotalLDCcountOneWeek = 0;

			try {
				ResultSet result3 = null;
				PreparedStatement pStatement3 = null;

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] fromTodayDateSplit = fromTodayDateConversion
						.split("-");
				String[] toTodayDateSplit = toTodayDateConversion.split("-");

				StringBuffer fromTodayDate = new StringBuffer();
				StringBuffer toTodayDate = new StringBuffer();

				fromTodayDate.append(fromTodayDateSplit[2])
						.append(fromTodayDateSplit[1])
						.append(fromTodayDateSplit[0]).append("08");

				toTodayDate.append(toTodayDateSplit[2])
						.append(toTodayDateSplit[1])
						.append(toTodayDateSplit[0]).append("08");

				StringBuffer fromYesterdayDate = new StringBuffer();
				StringBuffer toYesterdayDate = new StringBuffer();

				String toYesterdayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromYesterdayDateConversion = myDate.format(
						cal.getTime()).toString();

				String[] fromYesterdayDateSplit = fromYesterdayDateConversion
						.split("-");
				String[] toYesterdayDateSplit = toYesterdayDateConversion
						.split("-");

				fromYesterdayDate.append(fromYesterdayDateSplit[2])
						.append(fromYesterdayDateSplit[1])
						.append(fromYesterdayDateSplit[0]).append("08");

				toYesterdayDate.append(toYesterdayDateSplit[2])
						.append(toYesterdayDateSplit[1])
						.append(toYesterdayDateSplit[0]).append("08");

				StringBuffer fromLastWeekDate = new StringBuffer();
				StringBuffer toLastWeekDate = new StringBuffer();

				cal.add(Calendar.DAY_OF_MONTH, -5);
				String toLastWeekDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromLastWeekDateConversion = myDate
						.format(cal.getTime()).toString();

				String[] fromLastWeekDateSplit = fromLastWeekDateConversion
						.split("-");
				String[] toLastWeekDateSplit = toLastWeekDateConversion
						.split("-");

				fromLastWeekDate.append(fromLastWeekDateSplit[2])
						.append(fromLastWeekDateSplit[1])
						.append(fromLastWeekDateSplit[0]).append("08");

				toLastWeekDate.append(toLastWeekDateSplit[2])
						.append(toLastWeekDateSplit[1])
						.append(toLastWeekDateSplit[0]).append("08");

				Connection con3 = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the query for LDC every one hour Today");

				StringBuilder query3 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query3.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query3.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query3.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query3.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('LDC') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query3.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query3.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query3.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement3 = con3.prepareStatement(query3.toString());
				pStatement3.setString(1, fromTodayDate.toString());
				pStatement3.setString(2, toTodayDate.toString());

				result3 = pStatement3.executeQuery();
				System.out
						.println("Fetching the results after running the query for LDC Today");
				while (result3.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result3
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result3
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result3
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result3
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result3
							.getString("ORDER_DATE")));
					orderCountHourlyLDCToday.add(ordercountobj);
					TotalLDCcount += ordercountobj.getOrderCount();
				}
				totalOrdercountLDC = TotalLDCcount;
				con3.close();
				pStatement3.close();
				result3.close();
				String WareHouseSheetNameLDC = "Hourly Order Today ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyLDCToday,
						WareHouseSheetNameLDC);

				ResultSet result4 = null;
				PreparedStatement pStatement4 = null;

				Connection con4 = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the Comparison graph for LDC for every one hour Yesterday");
				StringBuilder query4 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query4.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query4.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query4.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query4.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('LDC') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query4.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query4.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query4.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement4 = con4.prepareStatement(query4.toString());
				pStatement4.setString(1, fromYesterdayDate.toString());
				pStatement4.setString(2, toYesterdayDate.toString());

				result4 = pStatement4.executeQuery();
				System.out
						.println("Fetching the results after running the query for LDC for every one hour Yesterday");

				while (result4.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result4
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result4
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result4
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result4
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result4
							.getString("ORDER_DATE")));
					orderCountHourlyLDCYesterday.add(ordercountobj);
					TotalLDCcountYesterday += ordercountobj.getOrderCount();
				}
				totalOrdercountLDCYesterday = TotalLDCcountYesterday;
				con4.close();
				pStatement4.close();
				result4.close();

				String WareHouseSheetNameLDCYesterday = "Hourly Order Yesterday ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyLDCYesterday,
						WareHouseSheetNameLDCYesterday);

				ResultSet result5 = null;
				PreparedStatement pStatement5 = null;

				Connection con5 = ConnectOWNPRD.getConnection();

				System.out
						.println("Running the Comparison graph query for LDC for every hour last week the same day");
				StringBuilder query5 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ count(DISTINCT REL.SHIP_ADVICE_NO) AS ORDER_COUNT,to_char(ST.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE, DES.Description AS Status_Desc,REL.shipnode_key ");
				query5.append(" FROM Dom.Yfs_Order_Release_Status ST JOIN Dom.Yfs_Order_Header HDR ON ST.Order_Header_Key = HDR.Order_Header_Key JOIN Dom.Yfs_Order_Line_Schedule OLS ");
				query5.append(" ON ST.Order_Line_Schedule_Key = OLS.Order_Line_Schedule_Key JOIN Dom.Yfs_Order_Line LINE ON ST.Order_Line_Key = LINE.Order_Line_Key LEFT JOIN DOM.YFS_ORDER_RELEASE REL ");
				query5.append(" ON ST.ORDER_RELEASE_KEY = REL.ORDER_RELEASE_KEY JOIN Dom.yfs_status DES ON DES.status = ST.status WHERE DES.process_type_key ='ORDER_FULFILLMENT' AND LINE.line_type = 'INLINE' ");
				query5.append(" AND HDR.document_type = '0001' AND ST.status  in('3200.03') AND REL.shipnode_key in ('LDC') AND HDR.ENTERPRISE_KEY = 'NIKEUS' ");
				query5.append(" AND ST.ORDER_RELEASE_STATUS_KEY > ? ");
				query5.append(" AND ST.ORDER_RELEASE_STATUS_KEY < ? ");
				query5.append(" Group by to_char(ST.status_date-8/24,'yyyy-mm-dd HH24'),DES.Description,REL.shipnode_key ORDER BY ORDER_DATE ");

				pStatement5 = con5.prepareStatement(query5.toString());
				pStatement5.setString(1, fromLastWeekDate.toString());
				pStatement5.setString(2, toLastWeekDate.toString());

				result5 = pStatement5.executeQuery();
				System.out
						.println("Fetching the results after running the query for LDC for every hour last week the same day");

				while (result5.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result5
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result5
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result5
							.getString("Status_Desc"));
					ordercountobj.setShipnodeKey(result5
							.getString("shipnode_key"));
					ordercountobj.setOrderDate(dateFormat2(result5
							.getString("ORDER_DATE")));
					orderCountHourlyLDCLastWeek.add(ordercountobj);
					TotalLDCcountOneWeek += ordercountobj.getOrderCount();
				}
				totalOrdercountLDCOneWeek = TotalLDCcountOneWeek;
				con5.close();
				pStatement5.close();
				result5.close();
				String WareHouseSheetNameLDCOneWeek = "Hourly Order LastWeek ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyLDCLastWeek,
						WareHouseSheetNameLDCOneWeek);
			} catch (SQLException e) {

				e.printStackTrace();
			} finally {

			}
		}

		else if (Warehouse.equalsIgnoreCase("NALC")) {
			int TotalNALCcount = 0;
			int TotalNALCcountYesterday = 0;
			int TotalNALCcountOneWeek = 0;
			try {
				ResultSet result = null;
				PreparedStatement pStatement = null;
				Connection con = ConnectOWNPRD.getConnection();

				SimpleDateFormat myDate = new SimpleDateFormat("dd-MM-yyyy");
				Date curDate = myDate.parse(getDate);

				Calendar cal = Calendar.getInstance();
				cal.setTime(curDate);

				String toTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromTodayDateConversion = myDate.format(cal.getTime())
						.toString();

				String[] fromTodayDateSplit = fromTodayDateConversion
						.split("-");
				String[] toTodayDateSplit = toTodayDateConversion.split("-");

				StringBuffer fromTodayDate = new StringBuffer();
				StringBuffer toTodayDate = new StringBuffer();

				fromTodayDate.append(fromTodayDateSplit[2])
						.append(fromTodayDateSplit[1])
						.append(fromTodayDateSplit[0]).append("08");

				toTodayDate.append(toTodayDateSplit[2])
						.append(toTodayDateSplit[1])
						.append(toTodayDateSplit[0]).append("08");

				StringBuffer fromYesterdayDate = new StringBuffer();
				StringBuffer toYesterdayDate = new StringBuffer();

				String toYesterdayDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromYesterdayDateConversion = myDate.format(
						cal.getTime()).toString();

				String[] fromYesterdayDateSplit = fromYesterdayDateConversion
						.split("-");
				String[] toYesterdayDateSplit = toYesterdayDateConversion
						.split("-");

				fromYesterdayDate.append(fromYesterdayDateSplit[2])
						.append(fromYesterdayDateSplit[1])
						.append(fromYesterdayDateSplit[0]).append("08");

				toYesterdayDate.append(toYesterdayDateSplit[2])
						.append(toYesterdayDateSplit[1])
						.append(toYesterdayDateSplit[0]).append("08");

				StringBuffer fromLastWeekDate = new StringBuffer();
				StringBuffer toLastWeekDate = new StringBuffer();

				cal.add(Calendar.DAY_OF_MONTH, -5);
				String toLastWeekDateConversion = myDate.format(cal.getTime())
						.toString();

				cal.add(Calendar.DAY_OF_MONTH, -1);

				String fromLastWeekDateConversion = myDate
						.format(cal.getTime()).toString();

				String[] fromLastWeekDateSplit = fromLastWeekDateConversion
						.split("-");
				String[] toLastWeekDateSplit = toLastWeekDateConversion
						.split("-");

				fromLastWeekDate.append(fromLastWeekDateSplit[2])
						.append(fromLastWeekDateSplit[1])
						.append(fromLastWeekDateSplit[0]).append("08");

				toLastWeekDate.append(toLastWeekDateSplit[2])
						.append(toLastWeekDateSplit[1])
						.append(toLastWeekDateSplit[0]).append("08");

				System.out
						.println("Running the query for NALC every one hour Today");
				// Statement pStatement = con.createStatement();
				StringBuilder query = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ COUNT(DISTINCT ORH.ORDER_NO) AS ORDER_COUNT,ST.DESCRIPTION AS Status_Desc,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE ");
				query.append(" FROM DOM.YFS_ORDER_RELEASE_STATUS ORS, DOM.YFS_ORDER_LINE ORL, DOM.YFS_ORDER_HEADER ORH, DOM.YFS_STATUS ST ");
				query.append(" WHERE ORS.ORDER_HEADER_KEY = ORH.ORDER_HEADER_KEY AND ORS.ORDER_LINE_KEY = ORL.ORDER_LINE_KEY AND ORS.STATUS = ST.STATUS ");
				query.append(" AND ST.PROCESS_TYPE_KEY = 'PO_FULFILLMENT' AND ORH.DOCUMENT_TYPE = '0005' ");
				query.append(" and ORS.ORDER_RELEASE_STATUS_KEY > ? and ORS.ORDER_RELEASE_STATUS_KEY < ? ");
				query.append(" AND ORS.STATUS <> '1400' and ors.status in ('1100.20') AND ORL.SHIPNODE_KEY LIKE '1014%' ");
				query.append(" AND ORL.LINE_TYPE = 'INLINE' ");
				query.append(" GROUP BY ST.DESCRIPTION,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') ORDER BY ORDER_DATE ");

				pStatement = con.prepareStatement(query.toString());
				pStatement.setString(1, fromTodayDate.toString());
				pStatement.setString(2, toTodayDate.toString());

				result = pStatement.executeQuery();
				System.out
						.println("Fetching the results after running the query for NALC for every hour Today");

				while (result.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result
							.getString("Status_Desc"));
					ordercountobj.setOrderDate(dateFormat2(result
							.getString("ORDER_DATE")));
					ordercountobj.setShipnodeKey("NALC");
					orderCountHourlyNALCToday.add(ordercountobj);
					TotalNALCcount += ordercountobj.getOrderCount();
				}
				totalOrdercountNALC = TotalNALCcount;
				con.close();
				pStatement.close();
				result.close();

				String WareHouseSheetNameNALC = "Hourly Order Today ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyNALCToday,
						WareHouseSheetNameNALC);

				ResultSet result1 = null;
				PreparedStatement pStatement1 = null;

				Connection con1 = ConnectOWNPRD.getConnection();
				System.out
						.println("Running the Comparison graph for NALC for every one hour Yesterday");
				// Statement pStatement = con.createStatement();
				StringBuilder query1 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ COUNT(DISTINCT ORH.ORDER_NO) AS ORDER_COUNT,ST.DESCRIPTION AS Status_Desc,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE ");
				query1.append(" FROM DOM.YFS_ORDER_RELEASE_STATUS ORS, DOM.YFS_ORDER_LINE ORL, DOM.YFS_ORDER_HEADER ORH, DOM.YFS_STATUS ST ");
				query1.append(" WHERE ORS.ORDER_HEADER_KEY = ORH.ORDER_HEADER_KEY AND ORS.ORDER_LINE_KEY = ORL.ORDER_LINE_KEY AND ORS.STATUS = ST.STATUS ");
				query1.append(" AND ST.PROCESS_TYPE_KEY = 'PO_FULFILLMENT' AND ORH.DOCUMENT_TYPE = '0005' ");
				query1.append(" and ORS.ORDER_RELEASE_STATUS_KEY > ? and ORS.ORDER_RELEASE_STATUS_KEY < ? ");
				query1.append(" AND ORS.STATUS <> '1400' and ors.status in ('1100.20') AND ORL.SHIPNODE_KEY LIKE '1014%' ");
				query1.append(" AND ORL.LINE_TYPE = 'INLINE' ");
				query1.append(" GROUP BY ST.DESCRIPTION,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') ORDER BY ORDER_DATE ");

				pStatement1 = con1.prepareStatement(query1.toString());
				pStatement1.setString(1, fromYesterdayDate.toString());
				pStatement1.setString(2, toYesterdayDate.toString());

				result1 = pStatement1.executeQuery();
				System.out
						.println("Fetching the results after running the query for NALC for very one hour Yesterday");

				while (result1.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result1
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result1
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result1
							.getString("Status_Desc"));
					ordercountobj.setOrderDate(dateFormat2(result1
							.getString("ORDER_DATE")));
					ordercountobj.setShipnodeKey("NALC");
					orderCountHourlyNALCYesterday.add(ordercountobj);
					TotalNALCcountYesterday += ordercountobj.getOrderCount();
				}
				totalOrdercountNALCYesterday = TotalNALCcountYesterday;
				con1.close();
				pStatement1.close();
				result1.close();

				String WareHouseSheetNameNALCYesterday = "Hourly Order Yesterday ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyNALCYesterday,
						WareHouseSheetNameNALCYesterday);

				ResultSet result2 = null;
				PreparedStatement pStatement2 = null;

				Connection con2 = ConnectOWNPRD.getConnection();
				System.out
						.println("Running the Comparison graph query for NALC for every hour last week the same day");
				// Statement pStatement = con.createStatement();
				StringBuilder query2 = new StringBuilder(
						" SELECT /*+PARALLEL(64)*/ COUNT(DISTINCT ORH.ORDER_NO) AS ORDER_COUNT,ST.DESCRIPTION AS Status_Desc,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') AS ORDER_DATE ");
				query2.append(" FROM DOM.YFS_ORDER_RELEASE_STATUS ORS, DOM.YFS_ORDER_LINE ORL, DOM.YFS_ORDER_HEADER ORH, DOM.YFS_STATUS ST ");
				query2.append(" WHERE ORS.ORDER_HEADER_KEY = ORH.ORDER_HEADER_KEY AND ORS.ORDER_LINE_KEY = ORL.ORDER_LINE_KEY AND ORS.STATUS = ST.STATUS ");
				query2.append(" AND ST.PROCESS_TYPE_KEY = 'PO_FULFILLMENT' AND ORH.DOCUMENT_TYPE = '0005' ");
				query2.append(" and ORS.ORDER_RELEASE_STATUS_KEY > ? and ORS.ORDER_RELEASE_STATUS_KEY < ? ");
				query2.append(" AND ORS.STATUS <> '1400' and ors.status in ('1100.20') AND ORL.SHIPNODE_KEY LIKE '1014%' ");
				query2.append(" AND ORL.LINE_TYPE = 'INLINE' ");
				query2.append(" GROUP BY ST.DESCRIPTION,to_char(ORS.status_date-8/24,'yyyy-mm-dd HH24') ORDER BY ORDER_DATE ");

				pStatement2 = con2.prepareStatement(query2.toString());
				pStatement2.setString(1, fromLastWeekDate.toString());
				pStatement2.setString(2, toLastWeekDate.toString());

				result2 = pStatement2.executeQuery();
				System.out
						.println("Fetching the results after running the query for NALC for every hour last week the same day");
				// String[] ShipnodeKey = { "NALC" };

				while (result2.next()) {
					OrderCountHourlyVO ordercountobj = new OrderCountHourlyVO();
					ordercountobj.setOrderCount(Integer.parseInt(result2
							.getString("ORDER_COUNT")));
					ordercountobj.setOrderTime(dateFormat(result2
							.getString("ORDER_DATE")));
					ordercountobj.setStatusDescription(result2
							.getString("Status_Desc"));
					ordercountobj.setOrderDate(dateFormat2(result2
							.getString("ORDER_DATE")));
					ordercountobj.setShipnodeKey("NALC");
					orderCountHourlyNALCLastWeek.add(ordercountobj);
					TotalNALCcountOneWeek += ordercountobj.getOrderCount();
				}
				totalOrdercountNALCOneWeek = TotalNALCcountOneWeek;
				con2.close();
				pStatement2.close();
				result2.close();

				String WareHouseSheetNameOneWeek = "Hourly Order LastWeek ("
						+ Warehouse + ")";
				writeDataToExcel(orderCountHourlyNALCLastWeek,
						WareHouseSheetNameOneWeek);

			} catch (SQLException e) {

				e.printStackTrace();
			} finally {

			}
		}
		List OrderCountWeek = new ArrayList();
		if (Warehouse.equalsIgnoreCase("BRD")) {
			OrderCountWeek.addAll(orderCountHourlyBRDToday);
			OrderCountWeek.addAll(orderCountHourlyBRDYesterday);
			OrderCountWeek.addAll(orderCountHourlyBRDLastWeek);
		} else if (Warehouse.equalsIgnoreCase("LDC")) {
			OrderCountWeek.addAll(orderCountHourlyLDCToday);
			OrderCountWeek.addAll(orderCountHourlyLDCYesterday);
			OrderCountWeek.addAll(orderCountHourlyLDCLastWeek);
		} else if (Warehouse.equalsIgnoreCase("NALC")) {
			OrderCountWeek.addAll(orderCountHourlyNALCToday);
			OrderCountWeek.addAll(orderCountHourlyNALCYesterday);
			OrderCountWeek.addAll(orderCountHourlyNALCLastWeek);
		}
		return OrderCountWeek;
	}

	public String dateFormat(String str_date) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH");
		SimpleDateFormat newformat = new SimpleDateFormat("HH:mm");

		Date date = formatter.parse(str_date);
		String resultDate = newformat.format(date);
		return resultDate;
	}

	public String dateFormat2(String str_date) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH");
		SimpleDateFormat newformat = new SimpleDateFormat("dd-MM-yyyy");

		Date date = formatter.parse(str_date);
		String resultDate = newformat.format(date);
		return resultDate;
	}

	public String getOrderCountTable() {
		// OrderPercentage orderPercentage = new OrderPercentage();
		String PercentageDifferenceBRD = OrderPercentage(totalOrdercountBRD,
				totalOrdercountBRDYesterday);
		String PercentageDifferenceLDC = OrderPercentage(totalOrdercountLDC,
				totalOrdercountLDCYesterday);
		String PercentageDifferenceNALC = OrderPercentage(totalOrdercountNALC,
				totalOrdercountNALCYesterday);

		String PercentageDifferenceLastWeekBRD = OrderPercentage(
				totalOrdercountBRD, totalOrdercountBRDOneWeek);
		String PercentageDifferenceLastWeekLDC = OrderPercentage(
				totalOrdercountLDC, totalOrdercountLDCOneWeek);
		String PercentageDifferenceLastWeekNALC = OrderPercentage(
				totalOrdercountNALC, totalOrdercountNALCOneWeek);

		String orderCounttable = "";
		orderCounttable += "<table border=1>";
		orderCounttable += "<tr bgcolor=\"#A4A4A4\" style=\"font-weight: bolder;\" align=\"center\">";
		orderCounttable += "<td>";
		orderCounttable += "DC";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += "Total Number Of Orders Today";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += "Total Number Of Orders Yesterday";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += "Difference in % (Today and Yesterday)";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += "Total Number Of Orders Last Week";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += "Difference in % (Today and Last Week)";
		orderCounttable += "</td>";
		orderCounttable += "</tr>";

		orderCounttable += "<tr style=\"font-weight: bolder;\" align=\"center\">";
		orderCounttable += "<td>";
		orderCounttable += "BRD";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountBRD;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountBRDYesterday;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceBRD;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountBRDOneWeek;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceLastWeekBRD;
		orderCounttable += "</td>";

		orderCounttable += "</tr>";

		orderCounttable += "<tr style=\"font-weight: bolder;\" align=\"center\">";
		orderCounttable += "<td>";
		orderCounttable += "SD1";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountLDC;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountLDCYesterday;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceLDC;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountLDCOneWeek;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceLastWeekLDC;
		orderCounttable += "</td>";

		orderCounttable += "</tr>";

		orderCounttable += "<tr style=\"font-weight: bolder;\" align=\"center\">";
		orderCounttable += "<td>";
		orderCounttable += "NALC";
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountNALC;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountNALCYesterday;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceNALC;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += totalOrdercountNALCOneWeek;
		orderCounttable += "</td>";
		orderCounttable += "<td>";
		orderCounttable += PercentageDifferenceLastWeekNALC;
		orderCounttable += "</td>";

		orderCounttable += "</tr>";

		orderCounttable += "</table>";
		return orderCounttable;
	}

	public static void createHeader(HSSFSheet mySheet) {
		short sh = 0;
		HSSFRow hourlyCountRow = mySheet.createRow(0);
		HSSFCell hourlyCountCell = hourlyCountRow.createCell(sh);
		hourlyCountCell.setCellValue("ORDER COUNT");
		hourlyCountCell = hourlyCountRow.createCell(++sh);
		hourlyCountCell.setCellValue("ORDER DATE");
		hourlyCountCell = hourlyCountRow.createCell(++sh);
		hourlyCountCell.setCellValue("ORDER TIME");
		hourlyCountCell = hourlyCountRow.createCell(++sh);
		hourlyCountCell.setCellValue("STATUS DESC");
		hourlyCountCell = hourlyCountRow.createCell(++sh);
		hourlyCountCell.setCellValue("SHIPNODE KEY");
	}

	public static void writeDataToExcel(List orderCountWeekExcel,
			String WareHouseSheetName) {
		int row = 0;
		HSSFSheet mySheet = hourlyCountWorkBook.createSheet(WareHouseSheetName);
		short defaultColWidth = 20;
		mySheet.setDefaultColumnWidth(defaultColWidth);
		short sh = 0;
		createHeader(mySheet);

		for (int i = 0; i < orderCountWeekExcel.size(); i++) {
			OrderCountHourlyVO obj = (OrderCountHourlyVO) orderCountWeekExcel
					.get(i);
			hourlyCountRow = mySheet.createRow(++row);
			hourlyCountCell = hourlyCountRow.createCell(sh);
			hourlyCountCell.setCellValue(obj.getOrderCount());
			hourlyCountCell = hourlyCountRow.createCell(++sh);
			hourlyCountCell.setCellValue(obj.getOrderDate());
			hourlyCountCell = hourlyCountRow.createCell(++sh);
			hourlyCountCell.setCellValue(obj.getOrderTime());
			hourlyCountCell = hourlyCountRow.createCell(++sh);
			hourlyCountCell.setCellValue(obj.getStatusDescription());
			hourlyCountCell = hourlyCountRow.createCell(++sh);
			hourlyCountCell.setCellValue(obj.getShipnodeKey());
			hourlyCountCell = hourlyCountRow.createCell(++sh);
			sh = 0;
		}
		FileOutputStream fileOut = null;
		try {
			String inputFile = "/cust/home/a.cdtpsauto/OrderReportAutomation/generateChart.properties";
			final Properties prop = new Properties();
			// ClassLoader classLoader = Thread.currentThread()
			// .getContextClassLoader();
			// InputStream inputStream = classLoader
			// .getResourceAsStream(inputFile);
			// prop.load(inputStream);

			prop.load(new FileInputStream(inputFile));
			String OutputLocation = prop.getProperty("outputLocation");
			Boolean flag = false;
			if (!flag) {
				FileOutputStream out1 = new FileOutputStream(new File(
						OutputLocation + "OrderCount_Hourly.xls"));
				hourlyCountWorkBook.write(out1);
				out1.close();
				System.out.println("File Generated Successfully!");
				flag = true;
			} else {
				System.out.println("File NOT Generated Successfully!");
			}

		} catch (Exception e) {
			// logger.error(e);
			e.printStackTrace();
		}
	}

	public static String OrderPercentage(int totalOrdercount,
			int totalOrdercountYesterday) {
		String returnCount = "";
		if (totalOrdercount > totalOrdercountYesterday) {
			int increase = (totalOrdercount - totalOrdercountYesterday) * 100;

			String increased = Integer.toString(increase
					/ totalOrdercountYesterday);
			returnCount = "Increased by " + increased + "%";

		} else if (totalOrdercount < totalOrdercountYesterday) {
			int decrease = (totalOrdercountYesterday - totalOrdercount) * 100;
			String decreased = Integer.toString(decrease
					/ totalOrdercountYesterday);
			returnCount = "Decreased by " + decreased + "%";
		}

		return returnCount;

	}
}
